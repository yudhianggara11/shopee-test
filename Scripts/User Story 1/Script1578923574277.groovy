import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//User Story - 1
// As a User I want to create a public gist.
WebUI.openBrowser('gist.github.com')

WebUI.click(findTestObject('Shopee Test Gist/Gist/Signin'))

WebUI.setText(findTestObject('Shopee Test Gist/Gist/Sign-in/Username Textbox'), GlobalVariable.username)

WebUI.setText(findTestObject('Shopee Test Gist/Gist/Sign-in/Password Textbox'), GlobalVariable.password)

WebUI.click(findTestObject('Shopee Test Gist/Gist/Sign-in/Signin Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl('gist.github.com')

WebUI.setText(findTestObject('Shopee Test Gist/Gist/User Story 1/Text Area Gist a'), 'Shopee keren. Bikin Ronaldo joget-joget receh')

WebUI.click(findTestObject('Shopee Test Gist/Gist/User Story 1/Create a public Gist Button'))

