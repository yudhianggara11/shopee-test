<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Shopee Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ba6b324a-b774-44fa-a68e-cb80bef8efad</testSuiteGuid>
   <testCaseLink>
      <guid>ccb62c7d-f044-490b-9f12-06aab3b20470</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User Story 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e9179ed-768e-461d-a950-0322eceb607a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User Story 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7da589f5-726e-4f91-ab6a-bb6380a40566</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User Story 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70075091-2915-4355-b2aa-d505d71de4b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User Story 4</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
